#include <QMessageBox>
#include <QString>
#include <QTransform>
#include <QtGui>
#include <bits/stdc++.h>
#include "glwidget.h"

using namespace std;

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent), alpha(0.05), log2_zoom(0),
      view_type(ViewType::ORTHO), hide_empty_points(true) {
  QSizePolicy size_policy;
  size_policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
  size_policy.setHorizontalPolicy(QSizePolicy::MinimumExpanding);
  setSizePolicy(size_policy);
}

GLWidget::~GLWidget() {}

float GLWidget::getAlpha() const { return alpha; }
double GLWidget::getWindowWidth() const { return win_width; }
double GLWidget::getWindowCenter() const { return win_center; }
int GLWidget::getActualSlice() const { return actual_slice; }
int GLWidget::getContourVal() const {return contour_val; }

void GLWidget::setAlpha(double new_alpha) {
  alpha = new_alpha;
  update();
}
void GLWidget::setWindowWidth(double new_win_width) {
  win_width = new_win_width;
  update();
}

void GLWidget::setWindowCenter(double new_win_center) {
  win_center = new_win_center;
  update();
}

void GLWidget::setActualSlice(double new_act_slice) {
  actual_slice = new_act_slice;
  update();
}

void GLWidget::updateVolumicData(std::unique_ptr<VolumicData> new_data) {
  volumic_data = std::move(new_data);
  updateDisplayPoints();
  update();
}
void GLWidget::updateDisPoint() {
  updateDisplayPoints();
  update();
}
void GLWidget::updateDisplayPoints() {
  display_points.clear();
  if (!volumic_data)
    return;
  int W = volumic_data->width;
  int H = volumic_data->height;
  int D = volumic_data->depth;
  int col = 0;
  int row = 0;
  int depth = 0;
  double x_factor = volumic_data->pixel_width;
  double y_factor = volumic_data->pixel_height;
  double z_factor = volumic_data->slice_spacing;
  double max_size =
      std::max(std::max(x_factor * W, y_factor * H), z_factor * D);
  double global_factor = 2.0 / max_size;
  x_factor *= global_factor;
  y_factor *= global_factor;
  z_factor *= global_factor;
  int idx_start = 0;
  int idx_end = W * H * D;
  hidden=0;
  if (idx_end - idx_start <= 0) {
    return;
  }
  display_points.reserve(idx_end - idx_start);
  // Importing points

  double min = win_center - (win_width / 2);
  double max = win_center + (win_width / 2);

  for (int idx = idx_start; idx < idx_end; idx++) {
    //double c = volumic_data->data2[idx] / 255.0;
    uint16_t raw_data = volumic_data->data2[idx];
    int c = volumic_data->BinarySegmentation(raw_data, min, max);
    c = connectivity(col, row, depth, W, H, c, min, max);
    if ((c > 0 && c != 2) || !hide_empty_points) {
      //int test = volumic_data->BinarySegmentation(raw_data,min,max);
      DrawablePoint p;
      p.c = c;
      p.pos = QVector3D((col - W / 2.) * x_factor, (row - H / 2.) * y_factor,
                        (depth - D / 2.) * z_factor);
      display_points.push_back(p);
    }
    col++;
    if (col == W) {
      row++;
      col = 0;
    }
    if (row == H) {
      depth++;
      row = 0;
    }
  }
  std::cout << "Nb points: " << display_points.size() << std::endl;
  std::cout << "Hidden points: " << hidden << std::endl;
}

void GLWidget::initializeGL() {
  glEnable(GL_BLEND);
  glDisable(GL_DEPTH_TEST);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDepthFunc(GL_NEVER);
}

void GLWidget::paintGL() {
  QSize viewport_size = size();
  int width = viewport_size.width();
  int height = viewport_size.height();
  double aspect_ratio = width / (float)height;
  glViewport(0, 0, width, height);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  switch (view_type) {
  case ViewType::ORTHO: {
    double view_half_size = std::pow(2, -log2_zoom);
    glScalef(1.0, aspect_ratio, 1.0);
    QVector3D center(0, 0, 0);
    glOrtho(center.x() - view_half_size, center.x() + view_half_size,
            center.y() - view_half_size, center.y() + view_half_size,
            center.z() - view_half_size, center.z() + view_half_size);
    glMultMatrixf(transform.constData());
    break;
  }
  case ViewType::FRUSTUM: {
    float near_dist = 0.5;
    float far_dist = 5.0;
    QMatrix4x4 projection;
    projection.perspective(90, aspect_ratio, near_dist, far_dist);
    QMatrix4x4 cam_offset;
    cam_offset.translate(0, 0, -2 * (1 - log2_zoom));
    QMatrix4x4 pov = projection * cam_offset * transform;
    glMultMatrixf(pov.constData());
  }
  }

  glMatrixMode(GL_MODELVIEW);

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();

  glBegin(GL_POINTS);
  for (const DrawablePoint &p : display_points) {
    double color[3] = {p.c, p.c, p.c};
    if(bMode)
      std::copy(std::begin(color1), std::end(color1), std::begin(color));
    glColor4f(color[0], color[0], color[0], alpha);
    glVertex3d(p.pos.x(), p.pos.y(), p.pos.z());
  }
  glEnd();
}

void GLWidget::mousePressEvent(QMouseEvent *event) { lastPos = event->pos(); }

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
  double dx = modifiedDelta(event->x() - lastPos.x());
  double dy = modifiedDelta(event->y() - lastPos.y());

  if (event->buttons() & Qt::LeftButton) {
    QQuaternion local_rotation =
        QQuaternion::fromEulerAngles(0.5 * dy, 0.5 * dx, 0);
    QMatrix4x4 local_matrix;
    local_matrix.rotate(local_rotation);
    transform = local_matrix * transform;
  } else if (event->buttons() & Qt::RightButton) {
    QMatrix4x4 local_matrix;
    local_matrix.translate(QVector3D(dx, -dy, 0) * 0.001);
    transform = local_matrix * transform;
  }

  lastPos = event->pos();
  update();
}

void GLWidget::wheelEvent(QWheelEvent *event) {
  double delta = modifiedDelta(event->angleDelta().y() / 1000.0);
  log2_zoom += delta;
  update();
}

double GLWidget::modifiedDelta(double delta) {
  if (QGuiApplication::keyboardModifiers() & Qt::ShiftModifier) {
    return 10 * delta;
  }
  return delta;
}

void GLWidget::contour0(bool isChecked)
{
  if(isChecked)
  {
    six_connectivity = false;
    heighteen_connectivity = false;
    twentysix_connectivity = false;
  }
  updateDisPoint();
}

void GLWidget::contour6(bool isChecked)
{
  if(isChecked){
      six_connectivity = true;
      heighteen_connectivity = false;
      twentysix_connectivity = false;
  }
  updateDisPoint();
}

void GLWidget::contour18(bool isChecked)
{
  if(isChecked){
      six_connectivity = false;
      heighteen_connectivity = true;
      twentysix_connectivity = false;
  }
  updateDisPoint();
}

void GLWidget::contour26(bool isChecked)
{
  if(isChecked){
      six_connectivity = false;
      heighteen_connectivity = false;
      twentysix_connectivity = true;
  }
  updateDisPoint();
}

double GLWidget::numberOfPoints(){
  return display_points.size();
}

void GLWidget::export_xyz(const std::string fileName){
  ofstream myfile;
  myfile.open (fileName);
  for(double i =0; i < numberOfPoints(); i++ ){
    myfile << display_points[i].pos.x() << " ";
    myfile << display_points[i].pos.y() << " ";
    myfile << display_points[i].pos.z() << "\n";
  }
  myfile.close();
}



int GLWidget::makeSixConnectivity(int col, int row, int depth, vector<tuple<int, int, int>> values_to_change, int W, int H, double min, double max, int c)
{
  // First step : check which of the neighbors are accessibles, and store them into our vector
  for(int i=-1; i<=1; i+=2)
    {
      if(volumic_data->is_in_data(col+i, row, depth))
        values_to_change.emplace_back(col+i, row, depth);
      if(volumic_data->is_in_data(col, row+i, depth))
        values_to_change.emplace_back(col, row+i, depth);
      if(volumic_data->is_in_data(col, row, depth+i))
        values_to_change.emplace_back(col, row, depth+i);
    }
    // Second step : if values of the neighbors are != 1, we stop
    for(auto tup : values_to_change)
    {
      int column = get<0>(tup);
      int r = get<1>(tup);
      int d = get<2>(tup);
      int idx = (column + r * W + d * W * H);
      uint16_t raw_data = volumic_data->data2[idx];
      if(volumic_data->BinarySegmentation(raw_data, min, max) != 1)
        return c;
    }
    // Last step : return 2 = successfull, neighbors have value 1, we can hide this voxel.
    return 2;
}

int GLWidget::makeHeighteenConnectivity(int col, int row, int depth, vector<tuple<int, int, int>> values_to_change, int W, int H, double min, double max, int c)
{
  values_to_change.clear();
  for(int i=-1; i<=1; i+=2)
  {
    if(volumic_data->is_in_data(col + i, row + i, depth))
      values_to_change.emplace_back(col + i, row + i, depth);
    if(volumic_data->is_in_data(col + i, row -i, depth))
      values_to_change.emplace_back(col + i, row - i, depth);
    if(volumic_data->is_in_data(col + i, row, depth + i))
      values_to_change.emplace_back(col + i, row, depth + i);
    if(volumic_data->is_in_data(col + i, row, depth - i))
      values_to_change.emplace_back(col + i, row, depth - i);
    if(volumic_data->is_in_data(col, row + i, depth + i))
      values_to_change.emplace_back(col, row + i, depth + i);
    if(volumic_data->is_in_data(col, row + i, depth - i))
      values_to_change.emplace_back(col, row + i, depth - i);
  }
  for(auto tup : values_to_change)
  {
    int column = get<0>(tup);
    int r = get<1>(tup);
    int d = get<2>(tup);
    int idx = (column + r * W + d * W * H);
    uint16_t raw_data = volumic_data->data2[idx];
    if(volumic_data->BinarySegmentation(raw_data, min, max) != 1){
      return c;
    }
  }
  return 2;
}

int GLWidget::makeTwentysix_connectivity(int col, int row, int depth, vector<tuple<int, int, int>> values_to_change, int W, int H, double min, double max, int c)
{
  values_to_change.clear();
  for(int i=-1; i<=1; i+=2)
  {
    if(volumic_data->is_in_data(col + i, row + i, depth + i))
      values_to_change.emplace_back(col + i, row + i, depth + i);
    if(volumic_data->is_in_data(col + i, row  + i, depth - i))
      values_to_change.emplace_back(col + i, row + i, depth - i);
    if(volumic_data->is_in_data(col + i, row - i, depth + i))
      values_to_change.emplace_back(col + i, row - i, depth + i);
    if(volumic_data->is_in_data(col -i, row + i, depth + i))
      values_to_change.emplace_back(col - i, row + i, depth + i);
  }
  for(auto tup : values_to_change)
  {
    int column = get<0>(tup);
    int r = get<1>(tup);
    int d = get<2>(tup);
    int idx = (column + r * W + d * W * H);
    uint16_t raw_data = volumic_data->data2[idx];
    if(volumic_data->BinarySegmentation(raw_data, min, max) != 1){
      return c;
    }
  }
  return 2;
}


int GLWidget::connectivity(int col, int row, int depth, int W, int H, int c, double min, double max)
{
  if(c!= 1)
    return c;
  vector<tuple<int, int, int>> values_to_change;
  if(six_connectivity){
    int co = makeSixConnectivity(col, row, depth, values_to_change, W, H, min, max, c);
    if(co == 2)
      hidden+=1;
    return co;
  }
  else if(heighteen_connectivity)
  {
    if(makeSixConnectivity(col, row, depth, values_to_change, W, H, min, max, c) == 2)
    {
      int co = makeHeighteenConnectivity(col, row, depth, values_to_change, W, H, min, max, c);
      if(co == 2)
        hidden+=1;
      return co;
    }
  }
  else if(twentysix_connectivity)
  {
    if(makeSixConnectivity(col, row, depth, values_to_change, W, H, min, max, c) == 2)
    {
      if(makeHeighteenConnectivity(col, row, depth, values_to_change, W, H, min, max, c) == 2)
      {
        int co = makeTwentysix_connectivity(col, row, depth, values_to_change, W, H, min, max, c);
        if(co == 2)
          hidden +=1;
        return co;
      }
    }
  }
  return c;
}

void GLWidget::saveRanges(QJsonArray r1, QJsonArray r2) {
  for(int i=0; i<r1.size(); i++)
  {
    range1[i] = r1[i].toInt();
    range2[i] = r2[i].toInt();
  }
}

void GLWidget::saveColors(QJsonArray c1, QJsonArray c2) {
  for(int i=0; i<c1.size(); i++)
  {
    color1[i] = c1[i].toDouble();
    color2[i] = c2[i].toDouble();
  }
}

void GLWidget::boneMode(bool isChecked) {
  if(isChecked)
  {
    setWindowWidth(range1[0]);
    setWindowCenter(range1[1]);
    bMode = true;
  }
  updateDisPoint();
}