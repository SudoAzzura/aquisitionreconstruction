#ifndef VOLUMIC_DATA_H
#define VOLUMIC_DATA_H

#include <vector>
#include <inttypes.h>

class VolumicData {
public:
  // The data from the volume stored:
  // - column by column
  // - line by line
  // - slice by slice
  std::vector<unsigned char> data;
  std::vector<uint16_t> data2;

  int width;
  int height;
  int depth;
  double w_min;
  double w_max;
  double pixel_width;
  double pixel_height;
  double slice_spacing;

  // The data provided
  VolumicData();
  VolumicData(int width, int height, int depth);
  VolumicData(int width, int height, int depth, double w_min, double w_max);
  VolumicData(const VolumicData &other);
  ~VolumicData();

  //unsigned char getValue(int col, int row, int layer);
  uint16_t getValue(int col, int row, int layer);
  void setValue(int col, int row, int layer, uint16_t val);
  void setLayer(unsigned char *layer_data, int layer);
  void setLayer(uint16_t *layer_data, int layer, double intercept);
  double manualSetWindow(double value);
  int BinarySegmentation(double value,double min,double max);
  bool not_on_edges(int col, int row, int layer);
  bool is_in_data(int col, int row, int layer);
};

#endif // VOLUMIC_DATA_H
