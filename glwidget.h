#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QMatrix4x4>
#include <QOpenGLWidget>
#include <QString>
#include <iostream>
#include <memory>
#include <vector>
#include "volumic_data.h"
#include <QJsonArray>

class GLWidget : public QOpenGLWidget {
public:
  Q_OBJECT
public:
  enum ViewType { ORTHO, FRUSTUM };
  GLWidget(QWidget *parent = 0);
  ~GLWidget();
  QSize sizeHint() const { return QSize(200, 200); }

  float getAlpha() const;
  double getWindowWidth() const;
  double getWindowCenter() const;
  int getActualSlice()const;
  int getContourVal() const;
  void updateDisPoint();
  void updateVolumicData(std::unique_ptr<VolumicData> new_data);
  double numberOfPoints();
  void export_xyz(const std::string fileName);
  int connectivity(int col, int row, int depth, int W, int H, int c, double min, double max);
  int makeSixConnectivity(int col, int row, int depth, std::vector<std::tuple<int, int, int>> values_to_change, int W, int H, double min, double max, int c);
  int makeHeighteenConnectivity(int col, int row, int depth, std::vector<std::tuple<int, int, int>> values_to_change, int W, int H, double min, double max, int c);
  int makeTwentysix_connectivity(int col, int row, int depth, std::vector<std::tuple<int, int, int>> values_to_change, int W, int H, double min, double max, int c);
  void saveRanges(QJsonArray r1, QJsonArray r2);
  void saveColors(QJsonArray c1, QJsonArray c2);
public slots:
  void setAlpha(double new_alpha);
  void setWindowWidth(double new_win_width);
  void setWindowCenter(double new_win_center);
  void setActualSlice(double new_slice);
  void contour0(bool isChecked);
  void contour6(bool isChecked);
  void contour18(bool isChecked);
  void contour26(bool isChecked);
  void boneMode(bool isChecked);
protected:
  struct DrawablePoint {
    QVector3D pos;
    double c;
  };

  void initializeGL() override;
  void paintGL() override;

  void updateDisplayPoints();

  void wheelEvent(QWheelEvent *event) override;

  void mousePressEvent(QMouseEvent *event) override;
  void mouseMoveEvent(QMouseEvent *event) override;

  /**
   * If 'shift' modifier is pressed, multiplies value by 10
   */
  double modifiedDelta(double delta);

  QPoint lastPos;
  float alpha;
  int actual_slice;
  double win_width;
  double win_center;
  int contour_val;
  /**
   * Zoom value using a log scale
   * - positive is zoom in
   * - negative is zoom out
   */
  float log2_zoom;
  QMatrix4x4 transform;

  ViewType view_type;
  int hidden=0;
  /// When enabled, all points with a drawing color = 0 are hidden
  bool hide_empty_points;
  bool six_connectivity=false;
  bool heighteen_connectivity=false;
  bool twentysix_connectivity=false;
  bool bMode = false;
  bool liquidMode = false;
  int range1[2];
  int range2[2];

  double color1[3];
  double color2[3];
  /// The data of all the slices stored in a single object
  std::unique_ptr<VolumicData> volumic_data;

  /// The points to be drawn
  std::vector<DrawablePoint> display_points;
};

#endif // GLWIDGET_H
