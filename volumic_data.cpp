#include "volumic_data.h"
#include <cmath>
#include <stdexcept>
#include <iostream>
VolumicData::VolumicData()
    : width(-1), height(-1), depth(-1), pixel_width(-1), pixel_height(-1),
      slice_spacing(0) {}
VolumicData::VolumicData(int W, int H, int D)
    : data(W * H * D), width(W), height(H), depth(D) {}
VolumicData::VolumicData(int W, int H, int D, double WMIN, double WMAX)
    : data2(W * H * D), width(W), height(H), depth(D), w_min(WMIN), w_max(WMAX) {}

VolumicData::VolumicData(const VolumicData &other)
    : data2(other.data2), width(other.width), height(other.height),
      depth(other.depth), pixel_width(other.pixel_width),
      pixel_height(other.pixel_height), slice_spacing(other.slice_spacing) {}

VolumicData::~VolumicData() {}

// unsigned char VolumicData::getValue(int col, int row, int layer) {
//   return data[(col + row * width + layer * width * height)];
// }

uint16_t VolumicData::getValue(int col, int row, int layer) {
  return data2[(col + row * width + layer * width * height)];
}

void VolumicData::setValue(int col, int row, int layer, uint16_t val)
{
  data2[(col + row * width + layer * width * height)] = val;
}

bool VolumicData::is_in_data(int col, int row, int layer) {
  if((col + row * width + layer * width * height) < width*height*depth && (col + row * width + layer * width * height) >= 0)
    return true;
  return false;
  }

bool VolumicData::not_on_edges(int col, int row, int layer)
{
  if(col-1 >= 0 && col+1 < width && row-1 >=0 && row+1 < height && layer-1 >= 0 && layer+1 < depth)
    return true;
  // int idx_end = width*height*depth;
  // int idx_left = ((col-1) + row * width + layer * width * height);
  // int idx_right = ((col+1) + row * width + layer * width * height);
  // int idx_above = ( col + (row-1) * width + layer * width * height);
  // int idx_below = ( col + (row+1) * width + layer * width * height);
  // int idx_front = ( col + row * width + (layer-1) * width * height);
  // int idx_back =  ( col + row * width + (layer+1) * width * height);
  // if(idx_left >=0 && idx_right <= idx_end && idx_above >=0 && idx_below <= idx_end && idx_front >=0 && idx_back <=idx_end)
  //   return true;
  return false;

}

void VolumicData::setLayer(unsigned char *layer_data, int layer) {
  if (layer >= depth)
    throw std::out_of_range(
        "Layer " + std::to_string(layer) +
        " is outside of volume (depth=" + std::to_string(depth) + ")");
  int offset = width * height * layer;
  for (int i = 0; i < width * height; i++) {
    data[i + offset] = layer_data[i];
  }
}

void VolumicData::setLayer(uint16_t *layer_data, int layer, double intercept) {
  if (layer >= depth)
    throw std::out_of_range(
        "Layer " + std::to_string(layer) +
        " is outside of volume (depth=" + std::to_string(depth) + ")");
  int offset = width * height * layer;
  double offset2 = pow(2,15) - intercept;
  for (int i = 0; i < width * height; i++) {
    data2[i + offset] = layer_data[i]-offset2;
  }
}

double VolumicData::manualSetWindow(double value) {
  if(value < w_min) {
    return 0;
  } else if(value > w_max) {
    return 1;
  } else {
    return (value - w_min)/(w_max-w_min);
  }
}

int VolumicData::BinarySegmentation(double value,double min,double max) {
  if(value >= min && value <= max) {
    return 1;
  } else {
    return 0;
  }
}